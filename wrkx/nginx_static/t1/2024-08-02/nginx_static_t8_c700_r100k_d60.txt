Running 1m test @ http://localhost/
  8 threads and 700 connections
  Thread calibration: mean lat.: 67.005ms, rate sampling interval: 325ms
  Thread calibration: mean lat.: 55.193ms, rate sampling interval: 370ms
  Thread calibration: mean lat.: 59.473ms, rate sampling interval: 280ms
  Thread calibration: mean lat.: 15.225ms, rate sampling interval: 77ms
  Thread calibration: mean lat.: 62.753ms, rate sampling interval: 271ms
  Thread calibration: mean lat.: 64.043ms, rate sampling interval: 382ms
  Thread calibration: mean lat.: 63.451ms, rate sampling interval: 265ms
  Thread calibration: mean lat.: 154.984ms, rate sampling interval: 908ms
  Thread Stats   Avg      Stdev     Max   +/- Stdev
    Latency   192.96ms  438.57ms   3.77s    90.78%
    Req/Sec    12.39k     2.56k   27.10k    77.14%
  Latency Distribution (HdrHistogram - Recorded Latency)
 50.000%   25.41ms
 75.000%  124.29ms
 90.000%  580.61ms
 99.000%    2.34s 
 99.900%    3.40s 
 99.990%    3.72s 
 99.999%    3.76s 
100.000%    3.77s 

  Detailed Percentile spectrum:
       Value   Percentile   TotalCount 1/(1-Percentile)

       0.029     0.000000            1         1.00
       2.421     0.100000       486810         1.11
       5.447     0.200000       973360         1.25
       9.567     0.300000      1460097         1.43
      15.559     0.400000      1946362         1.67
      25.407     0.500000      2433270         2.00
      33.023     0.550000      2676864         2.22
      43.775     0.600000      2919855         2.50
      59.487     0.650000      3162870         2.86
      83.583     0.700000      3406276         3.33
     124.287     0.750000      3649548         4.00
     157.823     0.775000      3771125         4.44
     205.823     0.800000      3892652         5.00
     269.823     0.825000      4014361         5.71
     333.823     0.850000      4135999         6.67
     427.263     0.875000      4257666         8.00
     493.567     0.887500      4318481         8.89
     580.607     0.900000      4379403        10.00
     674.815     0.912500      4440103        11.43
     784.383     0.925000      4501120        13.33
     916.479     0.937500      4561735        16.00
    1014.271     0.943750      4592207        17.78
    1140.735     0.950000      4622621        20.00
    1247.231     0.956250      4653106        22.86
    1320.959     0.962500      4683624        26.67
    1395.711     0.968750      4713827        32.00
    1478.655     0.971875      4728985        35.56
    1635.327     0.975000      4744159        40.00
    1758.207     0.978125      4759358        45.71
    1927.167     0.981250      4774541        53.33
    2103.295     0.984375      4789901        64.00
    2162.687     0.985938      4797380        71.11
    2224.127     0.987500      4805004        80.00
    2308.095     0.989062      4812630        91.43
    2367.487     0.990625      4820195       106.67
    2430.975     0.992188      4827937       128.00
    2461.695     0.992969      4831589       142.22
    2500.607     0.993750      4835468       160.00
    2551.807     0.994531      4839279       182.86
    2613.247     0.995313      4843082       213.33
    2701.311     0.996094      4846808       256.00
    2758.655     0.996484      4848680       284.44
    2797.567     0.996875      4850672       320.00
    2832.383     0.997266      4852570       365.71
    2861.055     0.997656      4854376       426.67
    2942.975     0.998047      4856263       512.00
    3051.519     0.998242      4857223       568.89
    3110.911     0.998437      4858177       640.00
    3182.591     0.998633      4859120       731.43
    3289.087     0.998828      4860064       853.33
    3407.871     0.999023      4861023      1024.00
    3471.359     0.999121      4861511      1137.78
    3502.079     0.999219      4861986      1280.00
    3530.751     0.999316      4862467      1462.86
    3561.471     0.999414      4862932      1706.67
    3590.143     0.999512      4863412      2048.00
    3604.479     0.999561      4863663      2275.56
    3618.815     0.999609      4863879      2560.00
    3633.151     0.999658      4864125      2925.71
    3647.487     0.999707      4864342      3413.33
    3663.871     0.999756      4864580      4096.00
    3674.111     0.999780      4864701      4551.11
    3684.351     0.999805      4864821      5120.00
    3694.591     0.999829      4864950      5851.43
    3702.783     0.999854      4865057      6826.67
    3713.023     0.999878      4865184      8192.00
    3717.119     0.999890      4865252      9102.22
    3721.215     0.999902      4865313     10240.00
    3725.311     0.999915      4865366     11702.86
    3729.407     0.999927      4865420     13653.33
    3733.503     0.999939      4865483     16384.00
    3735.551     0.999945      4865517     18204.44
    3737.599     0.999951      4865546     20480.00
    3739.647     0.999957      4865574     23405.71
    3741.695     0.999963      4865592     27306.67
    3743.743     0.999969      4865615     32768.00
    3745.791     0.999973      4865635     36408.89
    3747.839     0.999976      4865650     40960.00
    3749.887     0.999979      4865667     46811.43
    3751.935     0.999982      4865679     54613.33
    3753.983     0.999985      4865702     65536.00
    3753.983     0.999986      4865702     72817.78
    3756.031     0.999988      4865716     81920.00
    3756.031     0.999989      4865716     93622.86
    3758.079     0.999991      4865728    109226.67
    3758.079     0.999992      4865728    131072.00
    3760.127     0.999993      4865738    145635.56
    3760.127     0.999994      4865738    163840.00
    3760.127     0.999995      4865738    187245.71
    3762.175     0.999995      4865744    218453.33
    3764.223     0.999996      4865749    262144.00
    3764.223     0.999997      4865749    291271.11
    3764.223     0.999997      4865749    327680.00
    3766.271     0.999997      4865756    374491.43
    3766.271     0.999998      4865756    436906.67
    3766.271     0.999998      4865756    524288.00
    3766.271     0.999998      4865756    582542.22
    3766.271     0.999998      4865756    655360.00
    3768.319     0.999999      4865758    748982.86
    3768.319     0.999999      4865758    873813.33
    3770.367     0.999999      4865760   1048576.00
    3770.367     0.999999      4865760   1165084.44
    3770.367     0.999999      4865760   1310720.00
    3770.367     0.999999      4865760   1497965.71
    3772.415     0.999999      4865762   1747626.67
    3772.415     1.000000      4865762   2097152.00
    3772.415     1.000000      4865762   2330168.89
    3772.415     1.000000      4865762   2621440.00
    3772.415     1.000000      4865762   2995931.43
    3772.415     1.000000      4865762   3495253.33
    3772.415     1.000000      4865762   4194304.00
    3772.415     1.000000      4865762   4660337.78
    3774.463     1.000000      4865763   5242880.00
    3774.463     1.000000      4865763          inf
#[Mean    =      192.960, StdDeviation   =      438.572]
#[Max     =     3772.416, Total count    =      4865763]
#[Buckets =           27, SubBuckets     =         2048]
----------------------------------------------------------
  5843834 requests in 1.00m, 1.10GB read
Requests/sec:  97421.04
Transfer/sec:     18.86MB
