wrk -t4 -c100 -R100 -d60 -L http://localhost:8081
Running 1m test @ http://localhost:8081
  4 threads and 100 connections
  Thread calibration: mean lat.: 6.765ms, rate sampling interval: 43ms
  Thread calibration: mean lat.: 5.689ms, rate sampling interval: 34ms
  Thread calibration: mean lat.: 6.273ms, rate sampling interval: 38ms
  Thread calibration: mean lat.: 6.536ms, rate sampling interval: 41ms
  Thread Stats   Avg      Stdev     Max   +/- Stdev
    Latency     3.01ms    5.67ms  95.62ms   92.40%
    Req/Sec    24.81     63.62   303.00     87.99%
  Latency Distribution (HdrHistogram - Recorded Latency)
 50.000%    1.42ms
 75.000%    2.04ms
 90.000%    7.21ms
 99.000%   23.23ms
 99.900%   80.89ms
 99.990%   95.68ms
 99.999%   95.68ms
100.000%   95.68ms

  Detailed Percentile spectrum:
       Value   Percentile   TotalCount 1/(1-Percentile)

       0.201     0.000000            1         1.00
       0.798     0.100000          493         1.11
       0.989     0.200000          982         1.25
       1.146     0.300000         1472         1.43
       1.284     0.400000         1964         1.67
       1.419     0.500000         2455         2.00
       1.499     0.550000         2700         2.22
       1.585     0.600000         2944         2.50
       1.693     0.650000         3190         2.86
       1.837     0.700000         3434         3.33
       2.041     0.750000         3679         4.00
       2.247     0.775000         3805         4.44
       2.625     0.800000         3925         5.00
       3.595     0.825000         4048         5.71
       4.527     0.850000         4170         6.67
       5.835     0.875000         4294         8.00
       6.407     0.887500         4354         8.89
       7.207     0.900000         4415        10.00
       7.795     0.912500         4476        11.43
       8.831     0.925000         4538        13.33
       9.959     0.937500         4599        16.00
      10.655     0.943750         4630        17.78
      11.703     0.950000         4660        20.00
      12.927     0.956250         4691        22.86
      14.495     0.962500         4722        26.67
      15.911     0.968750         4752        32.00
      16.199     0.971875         4768        35.56
      17.071     0.975000         4783        40.00
      17.599     0.978125         4798        45.71
      18.319     0.981250         4814        53.33
      19.199     0.984375         4829        64.00
      19.807     0.985938         4837        71.11
      20.415     0.987500         4844        80.00
      21.215     0.989062         4852        91.43
      23.519     0.990625         4861       106.67
      23.999     0.992188         4867       128.00
      24.239     0.992969         4871       142.22
      24.351     0.993750         4875       160.00
      24.847     0.994531         4879       182.86
      29.439     0.995313         4883       213.33
      30.015     0.996094         4886       256.00
      30.223     0.996484         4888       284.44
      33.023     0.996875         4890       320.00
      46.303     0.997266         4892       365.71
      71.295     0.997656         4894       426.67
      72.063     0.998047         4896       512.00
      72.127     0.998242         4897       568.89
      72.191     0.998437         4898       640.00
      77.311     0.998633         4899       731.43
      80.895     0.998828         4900       853.33
      81.407     0.999023         4901      1024.00
      81.407     0.999121         4901      1137.78
      95.615     0.999219         4903      1280.00
      95.615     0.999316         4903      1462.86
      95.615     0.999414         4903      1706.67
      95.615     0.999512         4903      2048.00
      95.615     0.999561         4903      2275.56
      95.679     0.999609         4905      2560.00
      95.679     1.000000         4905          inf
#[Mean    =        3.014, StdDeviation   =        5.673]
#[Max     =       95.616, Total count    =         4905]
#[Buckets =           27, SubBuckets     =         2048]
----------------------------------------------------------
  6004 requests in 1.00m, 0.93MB read
Requests/sec:    100.06
Transfer/sec:     15.83KB
-------------------------------------------
wrk -t4 -c100 -R500 -d60 -L http://localhost:8081
Running 1m test @ http://localhost:8081
  4 threads and 100 connections
  Thread calibration: mean lat.: 1.534ms, rate sampling interval: 10ms
  Thread calibration: mean lat.: 1.448ms, rate sampling interval: 10ms
  Thread calibration: mean lat.: 1.425ms, rate sampling interval: 10ms
  Thread calibration: mean lat.: 1.422ms, rate sampling interval: 10ms
  Thread Stats   Avg      Stdev     Max   +/- Stdev
    Latency     1.85ms    2.99ms  51.97ms   93.84%
    Req/Sec   131.92    143.23     1.00k    90.82%
  Latency Distribution (HdrHistogram - Recorded Latency)
 50.000%    1.20ms
 75.000%    1.55ms
 90.000%    2.26ms
 99.000%   16.32ms
 99.900%   35.84ms
 99.990%   47.62ms
 99.999%   52.00ms
100.000%   52.00ms

  Detailed Percentile spectrum:
       Value   Percentile   TotalCount 1/(1-Percentile)

       0.117     0.000000            1         1.00
       0.653     0.100000         2494         1.11
       0.813     0.200000         4980         1.25
       0.945     0.300000         7486         1.43
       1.074     0.400000         9974         1.67
       1.195     0.500000        12464         2.00
       1.255     0.550000        13700         2.22
       1.318     0.600000        14957         2.50
       1.388     0.650000        16197         2.86
       1.464     0.700000        17437         3.33
       1.553     0.750000        18676         4.00
       1.607     0.775000        19299         4.44
       1.671     0.800000        19924         5.00
       1.754     0.825000        20548         5.71
       1.851     0.850000        21166         6.67
       1.977     0.875000        21791         8.00
       2.075     0.887500        22101         8.89
       2.261     0.900000        22411        10.00
       2.759     0.912500        22722        11.43
       3.799     0.925000        23033        13.33
       4.759     0.937500        23344        16.00
       5.283     0.943750        23500        17.78
       5.919     0.950000        23655        20.00
       6.691     0.956250        23811        22.86
       7.655     0.962500        23968        26.67
       8.607     0.968750        24122        32.00
       9.215     0.971875        24201        35.56
       9.815     0.975000        24278        40.00
      10.583     0.978125        24356        45.71
      11.551     0.981250        24434        53.33
      12.759     0.984375        24511        64.00
      13.735     0.985938        24550        71.11
      14.727     0.987500        24589        80.00
      15.799     0.989062        24628        91.43
      16.927     0.990625        24668       106.67
      18.047     0.992188        24706       128.00
      19.055     0.992969        24725       142.22
      20.415     0.993750        24745       160.00
      21.471     0.994531        24765       182.86
      22.463     0.995313        24785       213.33
      23.087     0.996094        24803       256.00
      23.887     0.996484        24814       284.44
      24.927     0.996875        24823       320.00
      26.223     0.997266        24832       365.71
      27.327     0.997656        24842       426.67
      28.575     0.998047        24852       512.00
      30.543     0.998242        24857       568.89
      31.215     0.998437        24862       640.00
      32.223     0.998633        24866       731.43
      32.959     0.998828        24871       853.33
      36.735     0.999023        24876      1024.00
      37.119     0.999121        24879      1137.78
      37.375     0.999219        24881      1280.00
      37.631     0.999316        24883      1462.86
      39.775     0.999414        24886      1706.67
      42.047     0.999512        24888      2048.00
      42.271     0.999561        24890      2275.56
      42.527     0.999609        24891      2560.00
      42.847     0.999658        24892      2925.71
      43.007     0.999707        24893      3413.33
      46.527     0.999756        24894      4096.00
      46.911     0.999780        24895      4551.11
      46.943     0.999805        24896      5120.00
      46.943     0.999829        24896      5851.43
      47.327     0.999854        24897      6826.67
      47.327     0.999878        24897      8192.00
      47.615     0.999890        24898      9102.22
      47.615     0.999902        24898     10240.00
      47.615     0.999915        24898     11702.86
      47.647     0.999927        24899     13653.33
      47.647     0.999939        24899     16384.00
      47.647     0.999945        24899     18204.44
      47.647     0.999951        24899     20480.00
      47.647     0.999957        24899     23405.71
      51.999     0.999963        24900     27306.67
      51.999     1.000000        24900          inf
#[Mean    =        1.846, StdDeviation   =        2.992]
#[Max     =       51.968, Total count    =        24900]
#[Buckets =           27, SubBuckets     =         2048]
----------------------------------------------------------
  30004 requests in 1.00m, 4.64MB read
Requests/sec:    500.05
Transfer/sec:     79.11KB
-------------------------------------------
wrk -t4 -c100 -R1000 -d60 -L http://localhost:8081
Running 1m test @ http://localhost:8081
  4 threads and 100 connections
  Thread calibration: mean lat.: 1.841ms, rate sampling interval: 10ms
  Thread calibration: mean lat.: 1.831ms, rate sampling interval: 10ms
  Thread calibration: mean lat.: 3.101ms, rate sampling interval: 13ms
  Thread calibration: mean lat.: 1.788ms, rate sampling interval: 10ms
  Thread Stats   Avg      Stdev     Max   +/- Stdev
    Latency     2.25ms    3.77ms  56.93ms   93.69%
    Req/Sec   263.18    328.76     2.08k    90.26%
  Latency Distribution (HdrHistogram - Recorded Latency)
 50.000%    1.35ms
 75.000%    1.83ms
 90.000%    3.80ms
 99.000%   21.30ms
 99.900%   42.05ms
 99.990%   47.36ms
 99.999%   56.96ms
100.000%   56.96ms

  Detailed Percentile spectrum:
       Value   Percentile   TotalCount 1/(1-Percentile)

       0.152     0.000000            1         1.00
       0.685     0.100000         4991         1.11
       0.884     0.200000        10001         1.25
       1.054     0.300000        14972         1.43
       1.208     0.400000        19960         1.67
       1.351     0.500000        24935         2.00
       1.425     0.550000        27419         2.22
       1.502     0.600000        29938         2.50
       1.591     0.650000        32404         2.86
       1.696     0.700000        34899         3.33
       1.833     0.750000        37388         4.00
       1.926     0.775000        38631         4.44
       2.038     0.800000        39885         5.00
       2.169     0.825000        41125         5.71
       2.353     0.850000        42372         6.67
       2.787     0.875000        43615         8.00
       3.227     0.887500        44239         8.89
       3.803     0.900000        44861        10.00
       4.431     0.912500        45485        11.43
       5.223     0.925000        46108        13.33
       6.075     0.937500        46731        16.00
       6.583     0.943750        47042        17.78
       7.131     0.950000        47353        20.00
       7.903     0.956250        47665        22.86
       9.007     0.962500        47976        26.67
      10.471     0.968750        48288        32.00
      11.191     0.971875        48444        35.56
      12.047     0.975000        48599        40.00
      13.159     0.978125        48756        45.71
      14.631     0.981250        48911        53.33
      16.575     0.984375        49067        64.00
      17.359     0.985938        49145        71.11
      18.351     0.987500        49222        80.00
      19.743     0.989062        49300        91.43
      22.655     0.990625        49378       106.67
      23.919     0.992188        49457       128.00
      25.423     0.992969        49495       142.22
      26.399     0.993750        49535       160.00
      27.215     0.994531        49573       182.86
      29.727     0.995313        49612       213.33
      32.271     0.996094        49651       256.00
      33.023     0.996484        49670       284.44
      33.471     0.996875        49690       320.00
      34.111     0.997266        49709       365.71
      36.863     0.997656        49729       426.67
      39.071     0.998047        49748       512.00
      39.263     0.998242        49759       568.89
      39.455     0.998437        49769       640.00
      39.647     0.998633        49777       731.43
      40.223     0.998828        49787       853.33
      42.143     0.999023        49797      1024.00
      42.687     0.999121        49802      1137.78
      43.039     0.999219        49807      1280.00
      43.231     0.999316        49812      1462.86
      43.391     0.999414        49816      1706.67
      43.551     0.999512        49821      2048.00
      43.647     0.999561        49824      2275.56
      43.711     0.999609        49827      2560.00
      43.743     0.999658        49828      2925.71
      43.839     0.999707        49831      3413.33
      44.223     0.999756        49833      4096.00
      44.479     0.999780        49835      4551.11
      44.767     0.999805        49836      5120.00
      44.863     0.999829        49837      5851.43
      45.311     0.999854        49838      6826.67
      45.535     0.999878        49839      8192.00
      47.359     0.999890        49840      9102.22
      47.967     0.999902        49841     10240.00
      47.967     0.999915        49841     11702.86
      48.543     0.999927        49842     13653.33
      48.543     0.999939        49842     16384.00
      53.791     0.999945        49843     18204.44
      53.791     0.999951        49843     20480.00
      53.791     0.999957        49843     23405.71
      53.823     0.999963        49844     27306.67
      53.823     0.999969        49844     32768.00
      53.823     0.999973        49844     36408.89
      53.823     0.999976        49844     40960.00
      53.823     0.999979        49844     46811.43
      56.959     0.999982        49845     54613.33
      56.959     1.000000        49845          inf
#[Mean    =        2.247, StdDeviation   =        3.768]
#[Max     =       56.928, Total count    =        49845]
#[Buckets =           27, SubBuckets     =         2048]
----------------------------------------------------------
  59741 requests in 1.00m, 9.23MB read
Requests/sec:    995.65
Transfer/sec:    157.52KB
-------------------------------------------
wrk -t4 -c100 -R3000 -d60 -L http://localhost:8081
Running 1m test @ http://localhost:8081
  4 threads and 100 connections
  Thread calibration: mean lat.: 1.453ms, rate sampling interval: 10ms
  Thread calibration: mean lat.: 1.398ms, rate sampling interval: 10ms
  Thread calibration: mean lat.: 1.422ms, rate sampling interval: 10ms
  Thread calibration: mean lat.: 1.478ms, rate sampling interval: 10ms
  Thread Stats   Avg      Stdev     Max   +/- Stdev
    Latency     5.20ms   11.30ms 130.37ms   92.54%
    Req/Sec   802.93    493.37     6.33k    69.04%
  Latency Distribution (HdrHistogram - Recorded Latency)
 50.000%    1.45ms
 75.000%    3.54ms
 90.000%   12.73ms
 99.000%   61.82ms
 99.900%  106.75ms
 99.990%  120.51ms
 99.999%  128.90ms
100.000%  130.43ms

  Detailed Percentile spectrum:
       Value   Percentile   TotalCount 1/(1-Percentile)

       0.080     0.000000            1         1.00
       0.662     0.100000        14998         1.11
       0.894     0.200000        29938         1.25
       1.086     0.300000        44913         1.43
       1.262     0.400000        59894         1.67
       1.447     0.500000        74871         2.00
       1.557     0.550000        82298         2.22
       1.705     0.600000        89782         2.50
       1.921     0.650000        97268         2.86
       2.313     0.700000       104732         3.33
       3.539     0.750000       112218         4.00
       4.415     0.775000       115954         4.44
       5.467     0.800000       119690         5.00
       6.711     0.825000       123440         5.71
       8.187     0.850000       127176         6.67
      10.247     0.875000       130911         8.00
      11.423     0.887500       132781         8.89
      12.727     0.900000       134651        10.00
      14.511     0.912500       136529        11.43
      16.431     0.925000       138398        13.33
      19.135     0.937500       140267        16.00
      21.039     0.943750       141199        17.78
      23.407     0.950000       142136        20.00
      25.743     0.956250       143072        22.86
      29.071     0.962500       144006        26.67
      33.279     0.968750       144939        32.00
      36.095     0.971875       145409        35.56
      39.231     0.975000       145872        40.00
      42.783     0.978125       146346        45.71
      47.071     0.981250       146808        53.33
      51.199     0.984375       147279        64.00
      53.567     0.985938       147511        71.11
      56.511     0.987500       147743        80.00
      59.647     0.989062       147976        91.43
      63.487     0.990625       148210       106.67
      68.095     0.992188       148444       128.00
      70.655     0.992969       148567       142.22
      73.023     0.993750       148678       160.00
      76.287     0.994531       148798       182.86
      80.127     0.995313       148913       213.33
      84.095     0.996094       149029       256.00
      86.463     0.996484       149088       284.44
      88.831     0.996875       149146       320.00
      91.455     0.997266       149204       365.71
      94.463     0.997656       149262       426.67
      97.983     0.998047       149320       512.00
     100.223     0.998242       149350       568.89
     102.079     0.998437       149379       640.00
     103.487     0.998633       149409       731.43
     104.831     0.998828       149437       853.33
     106.879     0.999023       149466      1024.00
     108.095     0.999121       149481      1137.78
     109.055     0.999219       149498      1280.00
     109.695     0.999316       149511      1462.86
     110.143     0.999414       149525      1706.67
     111.167     0.999512       149541      2048.00
     112.383     0.999561       149547      2275.56
     112.831     0.999609       149555      2560.00
     113.727     0.999658       149562      2925.71
     114.815     0.999707       149569      3413.33
     115.839     0.999756       149576      4096.00
     116.159     0.999780       149580      4551.11
     116.607     0.999805       149583      5120.00
     117.311     0.999829       149587      5851.43
     118.783     0.999854       149591      6826.67
     119.871     0.999878       149594      8192.00
     120.511     0.999890       149597      9102.22
     120.575     0.999902       149598     10240.00
     120.831     0.999915       149600     11702.86
     121.407     0.999927       149602     13653.33
     121.599     0.999939       149603     16384.00
     122.623     0.999945       149604     18204.44
     123.775     0.999951       149605     20480.00
     124.159     0.999957       149606     23405.71
     124.351     0.999963       149607     27306.67
     126.399     0.999969       149608     32768.00
     126.399     0.999973       149608     36408.89
     126.911     0.999976       149609     40960.00
     126.911     0.999979       149609     46811.43
     127.743     0.999982       149610     54613.33
     127.743     0.999985       149610     65536.00
     127.743     0.999986       149610     72817.78
     128.895     0.999988       149611     81920.00
     128.895     0.999989       149611     93622.86
     128.895     0.999991       149611    109226.67
     128.895     0.999992       149611    131072.00
     128.895     0.999993       149611    145635.56
     130.431     0.999994       149612    163840.00
     130.431     1.000000       149612          inf
#[Mean    =        5.198, StdDeviation   =       11.295]
#[Max     =      130.368, Total count    =       149612]
#[Buckets =           27, SubBuckets     =         2048]
----------------------------------------------------------
  179845 requests in 1.00m, 27.79MB read
Requests/sec:   2997.42
Transfer/sec:    474.20KB
-------------------------------------------
