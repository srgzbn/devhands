install-pip:
	sudo apt update
	sudo apt install python3-venv python3-pip

install-ansible:
	python3 -m pip install --user ansible-core
